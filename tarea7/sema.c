#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include "utils.h"
#include "sema.h"

Semaphore *make_semaphore(int value)
{
  Semaphore *sema = malloc(sizeof(Semaphore));
  int n = sem_init(sema, 0, value);
  if (n != 0) perror_exit("sem_init failed");
  return sema;
}

void semaphore_wait(Semaphore *sema)
{
  int n = sem_wait(sema);
  if (n != 0) perror_exit("sem_wait failed");
}

void semaphore_signal(Semaphore *sema)
{
  int n = sem_post(sema);
  if (n != 0) perror_exit("sem_post failed");
}