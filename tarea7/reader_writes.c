#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include "utils.c"
#include "sema.c"
#include <unistd.h>
#define SIZE 5         

typedef struct
{
  int buf[SIZE];   
  int in;               
  int out;              
  Semaphore *full;
  Semaphore *empty;
  pthread_mutex_t mutex;
} Buffer;

Buffer data;


void *Writer(void *arg)
{
  int item=0, index;

  index = (int)arg;


  while(1)
  {
    item++;

    semaphore_wait(data.empty);
    pthread_mutex_lock(&data.mutex);
    data.buf[data.in] = item;
    data.in = (data.in+1)%SIZE;
    printf("Escritor %d escribiendo %d\n", index, item);
    fflush(stdout);
    pthread_mutex_unlock(&data.mutex);
    semaphore_signal(data.full);

    sleep(1);
  }
  return NULL;
}

void *Reader(void *arg)
{
  int item=0, index;

  index = (int)arg;
  while(1)
  {
    semaphore_wait(data.full);
    pthread_mutex_lock(&data.mutex);
    item++;
    item=data.buf[data.out];
    data.out = (data.out+1)%SIZE;
    printf("Lector %d leyendo  %d\n", index, item);
    fflush(stdout);
    pthread_mutex_unlock(&data.mutex);
    semaphore_signal(data.empty);
    sleep(1);
  }
  return NULL;
}

int main()
{
  pthread_t idP, idC;
  int index;

  data.full = make_semaphore(0);
  data.empty = make_semaphore(SIZE);
  pthread_mutex_init(&data.mutex, NULL);
  for (index = 0; index < 3; index++)
  {
    pthread_create(&idP, NULL, Writer, (void*)index);
  }
  for(index=0; index<3; index++)
  {
    pthread_create(&idC, NULL, Reader, (void*)index);
  }

  pthread_exit(NULL);
}