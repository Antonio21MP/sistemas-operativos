#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>

void* readers(void* a);
void* writer(void* a);

sem_t readersem, writersem;
int data = 0;
int count = 0;
int main()
{
	sem_init(&readersem, 0, 1);
    sem_init(&writersem, 0, 1);
    pthread_t rtid[10];
	pthread_t wtid[10];

    for(int i=0;i<=9;i++)
  {
    pthread_create(&wtid[i],NULL,writer,(void *)i);
    pthread_create(&rtid[i],NULL,readers,(void *)i);
  }
  for(int i=0;i<=9;i++)
  {
    pthread_join(wtid[i],NULL);
    pthread_join(rtid[i],NULL);
  }

}
void *readers(void *a)
{
  int f;
  f = ((int)a);
  sem_wait(&readersem);
  count = count + 1;
  if(count==1)
   sem_wait(&writersem);
  sem_post(&readersem);
  printf("lector %d leyendo %d\n",f,data);
  sleep(1);
  sem_wait(&readersem);
  count = count - 1;
  if(count==0)
   sem_post(&writersem);
  sem_post(&readersem);
}

void *writer(void *a)
{
  int f;
  f = ((int) a);
  sem_wait(&writersem);
  data++;
  printf("Escritor %d escribiendo %d\n",f,data);
  sleep(1);
  sem_post(&writersem);
}