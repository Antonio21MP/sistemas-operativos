#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <time.h>
#include "sema.c"
#include "utils.c"
#define SIZE 5           

typedef struct
{
  int buf[SIZE];  
  int in;
  int out;
  Semaphore *full;
  Semaphore *empty;
  pthread_mutex_t mutex;
} Buffer;

Buffer slots;


void *Producer(void *arg)
{
  int item=0, index;

  index = (int)arg;


  while(1)
  {
    item++;
    
    semaphore_wait(slots.empty);
    pthread_mutex_lock(&slots.mutex);
    slots.buf[slots.in] = item;
    slots.in = (slots.in+1)%SIZE;
    printf("Poductor %d produciendo %d\n", index, item);
    fflush(stdout);
    pthread_mutex_unlock(&slots.mutex);
    semaphore_signal(slots.full);

    sleep(1);
  }
  return NULL;
}

void *Consumer(void *arg)
{
  int item=0, index;

  index = (int)arg;
  while(1)
  {
    semaphore_wait(slots.full);
    pthread_mutex_lock(&slots.mutex);
    item++;
    item=slots.buf[slots.out];
    slots.out = (slots.out+1)%SIZE;
    printf("Cosumidor %d consumiendo %d\n", index, item);
    fflush(stdout);
    pthread_mutex_unlock(&slots.mutex);
    semaphore_signal(slots.empty);

    sleep(1);
  }
  return NULL;
}

int main()
{
  pthread_t idP, idC;
  int index;

  slots.full = make_semaphore(0);
  slots.empty = make_semaphore(SIZE);
  pthread_mutex_init(&slots.mutex, NULL);
  for (index = 0; index < 3; index++)
  {
    pthread_create(&idP, NULL, Producer, (void*)index);
  }
  for(index=0; index<3; index++)
  {
    pthread_create(&idC, NULL, Consumer, (void*)index);
  }
  
  pthread_exit(NULL);
}
