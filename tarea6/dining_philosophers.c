# include<stdio.h>
# include<pthread.h>
# include<stdlib.h>
# include<unistd.h>
# include<ctype.h>
# include<sys/types.h>
# include<sys/wait.h>
# include<semaphore.h>
# include<sys/sem.h>

sem_t chopstick[100];
int n = 5;
void *thread_func(int no)
{

for(int i=0;i<5;++i)
{

    sem_wait(&chopstick[no]);
    sem_wait(&chopstick[(no+1)%n]);
    printf("\nFilosofo %d esta comiendo",no);

    sleep(1);

    printf("\nFilosofo %d termino de comer\n",no);
    sem_post(&chopstick[no]);
    sem_post(&chopstick[(no+1)%n]);
}

pthread_exit(NULL);
}

int main()
{

pthread_t a_thread[100];

for(int i=0;i<n;++i)
{
    sem_init(&chopstick[i],0,0);
}

for(int i=0;i<n;++i)
{
    pthread_create(&a_thread[i],NULL,thread_func,(int*) i);

sem_post(&chopstick[i]);
}

for(int i=0;i<n;++i)
{
    pthread_join(a_thread[i],NULL);
}


for(int i=0;i<n;++i)
{
    sem_destroy(&chopstick[i]);
}
exit(0);
}