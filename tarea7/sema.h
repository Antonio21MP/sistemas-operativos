typedef sem_t Semaphore;

Semaphore *make_semaphore(int value);
void semaphore_wait(Semaphore *sema);
void semaphore_signal(Semaphore *sema);
