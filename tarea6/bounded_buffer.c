#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <time.h>

void* producer(void*);
void* consumer(void*);

int buffer[5];
int init = 0;
int end = 0;

sem_t full, empty;

int main(int argc, char const *argv[]){
    sem_init(&full, 0, 0);
    sem_init(&empty, 0, 1);

    pthread_t tid[2];
	pthread_attr_t attr;
	pthread_attr_init(&attr);

	pthread_create(&tid[0], &attr, producer, NULL);
	pthread_create(&tid[1], &attr, consumer, NULL);

	pthread_join(tid[0], 0);
	pthread_join(tid[1], 0);
	return 0;

}
void* producer(void* a){
    int r=0;
    while(1){
        //srand(time(NULL));   // should only be called once
        r = rand()%100;
        sem_wait(&empty);
        printf("produciendo %d \n", r);
        buffer[init] = r;
        init = (init + 1) % 5;
        if(init == 0){
            sem_post(&full);
            sleep(1);
        }
        sem_post(&empty);
    }
}

void* consumer(void* b){
    int get = 0;
    while(1){
        sem_wait(&full);
        get = buffer[end];
        printf("cosumiendo %d \n", get);
        end = (end + 1) % 5;
        if(end == 0){
            sem_post(&empty);
            sleep(1);
        }
        sem_post(&full);
    }
}
