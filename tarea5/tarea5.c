#include <pthread.h>
#include <stdio.h>
#define SIZE 8
#define TOTALSIZE 16

int *result;
void *runner(void *param);
void *unions(void *param);
void printArray(int pos);
int limit;
int main(int argc, char *argv[])
{
	pthread_t tid;
	pthread_attr_t attr;

	if(argc == TOTALSIZE){
		fprintf(stderr, "usage: a.out <integer value>\n" );;
		return -1;
	}
    for(int i=1; i<=TOTALSIZE; i++){
        if(atoi(argv[i])<0){
            fprintf(stderr, "%d must be >= 0\n", atoi(argv[i]));
            return -1;		
        }
    }
    int array[TOTALSIZE];
    for(int i=1; i<=TOTALSIZE; i++){
	    /*printf("argv [%d", i);
        printf("]: %d\n", atoi(argv[i]));
        */
        array[i-1] = atoi(argv[i]);
        /*
        printf("array [%d", i-1);
        printf("]: %d\n", array[i-1]);
        */
    }
    //first
    pthread_attr_init(&attr);
	pthread_create(&tid, &attr, runner, (void*)array);
	pthread_join(tid, NULL);
    //second
    printArray(2);
    int* secondA = result;
    limit = 4;
    pthread_attr_init(&attr);
	pthread_create(&tid, &attr, unions, (void*)secondA);
	pthread_join(tid, NULL);
    secondA = result;
    printArray(4);
    //third
    limit = 8;
    pthread_attr_init(&attr);
	pthread_create(&tid, &attr, unions, (void*)secondA);
	pthread_join(tid, NULL);
    secondA = result;
    printArray(8);
    //final
    limit = 16;
    pthread_attr_init(&attr);
	pthread_create(&tid, &attr, unions, (void*)secondA);
	pthread_join(tid, NULL);
    printArray(16);
    
}
void *runner(void *param)
{
    int *tmp = (int*)param;
    for(int i=0; i<(TOTALSIZE); i++){
        if(tmp[i] > tmp[i+1]){
            int x = tmp[i];
            tmp[i] = tmp[i+1];
            tmp[i+1] = x;
            
        }
        /*printf("array [%d]", i);
        printf("(%d, ", tmp[i]);
        printf("%d)\n", tmp[i+1]);*/
        i += 1;
    }
    result = tmp; 

    /*int array1[SIZE];
    int array2[SIZE];
    for(int i=0; i<SIZE; i++){
        array1[i] = tmp[i];
    }
    for(int i=8; i<TOTALSIZE; i++){
        array2[i] = tmp[i];
    }*/
    
	pthread_exit(0);
}
void *unions(void *param){
    int *tmp = (int*)param;
    /*printf("\narray:");
    for(int i=0; i<TOTALSIZE;i++){
        printf(" %d ", tmp[i]);
    }*/
    switch(limit){
        case 4: // union 4 veces
            printf("union 4\n");
            for(int i=0; i<TOTALSIZE; i++){
                if(tmp[i] > tmp[i+2]){
                    int x = tmp[i];
                    tmp[i] = tmp[i+2];
                    tmp[i+2] = x;
                    if(tmp[i+1] > tmp[i+3]){
                        int y = tmp[i+1];
                        tmp[i+1] = tmp[i+3];
                        tmp[i+3] = x; 
                    }
                }else if(tmp[i] > tmp[i+3]){
                    int x = tmp[i];
                    tmp[i] = tmp[i+3];
                    tmp[i+3] = x;
                    if(tmp[i+1] > tmp[i+2]){
                        int y = tmp[i+1];
                        tmp[i+1] = tmp[i+2];
                        tmp[i+2] = x; 
                    }
                }
                
                if(tmp[i+1] > tmp[i+2]){
                    int x = tmp[i+1];
                    tmp[i+1] = tmp[i+2];
                    tmp[i+2] = x;
                    if(tmp[i] > tmp[i+3]){
                        int y = tmp[i];
                        tmp[i] = tmp[i+3];
                        tmp[i+2] = x; 
                    }
                }
                else if(tmp[i+1] > tmp[i+3]){
                    int x = tmp[i+1];
                    tmp[i+1] = tmp[i+3];
                    tmp[i+3] = x;
                    if(tmp[i] > tmp[i+2]){
                        int y = tmp[i];
                        tmp[i] = tmp[i+2];
                        tmp[i+2] = x; 
                    }
                }

                if(tmp[i] > tmp[i+1]){
                    int x = tmp[i];
                    tmp[i] = tmp[i+1];
                    tmp[i+1] = x;   
                }
                if(tmp[i+2] > tmp[i+3]){
                    int x = tmp[i+2];
                    tmp[i+2] = tmp[i+3];
                    tmp[i+3] = x;
                    
                }
                
                i+= 3;
            }

            break;
        case 8: // union 2 veces
            printf("union 2\n");
            int init = 0;
            int end = 8;
            for(int i=0; i<2; i++){
                //printf("vuelta: %d\n", i);
                for(int j=init; j<(end-4); j++){
                    for(int k=(init+4); k<end; k++){
                        /*printf("a1: %d -", tmp[j]);
                        printf(" a2: %d\n", tmp[k]);
                        */
                        if(tmp[j] > tmp[k]){
                            int x = tmp[j];
                            tmp[j] = tmp[k];
                            tmp[k] = x;
                        }else{
                            break;
                        }
                    }
                    //printf("next\n");
                }
                for(int l=(end-4); l<end; l++){
                    for(int j=(end-4); j< end; j++){
                        if((j+1) < end){
                            if(tmp[j] > tmp[j+1]){
                                    int x = tmp[j];
                                    tmp[j] = tmp[j+1];
                                    tmp[j+1] = x;
                            }
                        }
                    }
                }
                init = 8;
                end = 16;
            }
            break;

        case 16: // union 1 ultima vez
            printf("final union\n");
            init = 0;
            end = 16;
            for(int i=0; i<2; i++){
                //printf("vuelta: %d\n", i);
                for(int j=init; j<(end-8); j++){
                    for(int k=(init+8); k<end; k++){
                        /*printf("a1: %d -", tmp[j]);
                        printf(" a2: %d\n", tmp[k]);
                        */
                        if(tmp[j] > tmp[k]){
                            int x = tmp[j];
                            tmp[j] = tmp[k];
                            tmp[k] = x;
                        }else{
                            break;
                        }
                    }
                    //printf("next\n");
                }
                for(int l=(end-8); l<end; l++){
                    for(int j=(end-8); j< end; j++){
                        /*printf("a1: %d -", tmp[j]);
                        printf(" a2: %d\n", tmp[j+1]);
                        */
                        if((j+1) < end){
                            if(tmp[j] > tmp[j+1]){
                                    int x = tmp[j];
                                    tmp[j] = tmp[j+1];
                                    tmp[j+1] = x;
                            }
                        }
                    }
                }
            }
            break;
    }
    result = tmp;
    pthread_exit(0);
}
void printArray(int pos){
    printf("array:(");
    int cont = 0;
    for(int i=0; i<TOTALSIZE;i++){
        printf(" %d ", result[i]);
        cont++;
        if(cont == pos){
            printf(") , (");
            cont = 0;
        }
    }
    printf(")\n");
}